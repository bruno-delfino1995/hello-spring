package guru.springframework.webapp.bootstrap;

import guru.springframework.webapp.model.Author;
import guru.springframework.webapp.model.Book;
import guru.springframework.webapp.model.Publisher;
import guru.springframework.webapp.repository.AuthorRepository;
import guru.springframework.webapp.repository.BookRepository;
import guru.springframework.webapp.repository.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;


    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Author eric = new Author("Eric", "Evans");
        Publisher harper = new Publisher("Harper Collins", "asdf");
        Book ddd = new Book("Domain Driven Design", "1651234", harper);
        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);

        Author rod = new Author("Rod", "Johnson");
        Publisher worx = new Publisher("Worx", "qwer");
        Book noEJB = new Book("J2EE Development without EJB", "7182364", worx);
        rod.getBooks().add(noEJB);
        noEJB.getAuthors().add(rod);

        publisherRepository.save(harper);
        publisherRepository.save(worx);
        authorRepository.save(eric);
        authorRepository.save(rod);
        bookRepository.save(ddd);
        bookRepository.save(noEJB);
    }
}
