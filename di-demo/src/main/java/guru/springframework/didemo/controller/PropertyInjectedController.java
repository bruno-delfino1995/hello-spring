package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class PropertyInjectedController {
    // As Spring needs to detect on which properties/setters/constructors it needs to act upon to inject the registered
    // beans, and that can't be out of the blue, we need to annotate each field as elegible for Autowiring with @Autowired
    @Autowired
    // When we have more than one implementation for the same kind, Spring wouldn't be able to instantiate the class because
    // there's no clear resolution between the many implementations, to overcome this we use @Qualifier, which is specified
    // both in the dependency and in the class that need it, the dependency specifies what's its qualifier and the user
    // uses the same, which by default is the class name in lowerCase
    @Qualifier("greetingServiceImpl")
    public GreetingService service;

    public String sayHello() {
        return service.greeting();
    }
}
