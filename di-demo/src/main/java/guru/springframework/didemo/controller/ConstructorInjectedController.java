package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class ConstructorInjectedController {
    private GreetingService service;

    // On constructor based injection we don't need to annotate the constructor since Spring 4.2, now it detects and does
    // the automatic wiring without the clues that @Autowire gives. However, we could annotate the constructor yet, I just
    // need to confirm if there's any benefit on that
    public ConstructorInjectedController(@Qualifier("nice") GreetingService service) {
        this.service = service;
    }

    public String sayHello() {
        return service.greeting();
    }
}
