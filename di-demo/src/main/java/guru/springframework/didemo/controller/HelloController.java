package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingService;
import org.springframework.stereotype.Controller;

@Controller
public class HelloController {
    private GreetingService service;

    public HelloController(GreetingService service) {
        this.service = service;
    }

    public String hello() {
        return service.greeting();
    }
}
