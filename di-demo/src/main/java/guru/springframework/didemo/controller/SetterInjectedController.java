package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class SetterInjectedController {
    private GreetingService service;

    // For setters it goes the same as for properties but instead of annotating the property we annotate the setter, mainly
    // because the property is private and we don't want Spring to use the reflection mechanism to just inject our dependency
    @Autowired
    // Here I've defined a specific Qualifier, which is the preferred way, because by using the class name we would be semantically
    // bound to that class, while using a specific name we're bound to a purpose, like here, a greeting that is qualified as bad
    @Qualifier("bad") // This could also be specified with a parameter annotation as in ConstructorInjectedController
    public void setService(GreetingService service) {
        this.service = service;
    }

    public String sayHello() {
        return service.greeting();
    }
}
