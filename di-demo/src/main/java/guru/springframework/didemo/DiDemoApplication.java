package guru.springframework.didemo;

import guru.springframework.didemo.controller.ConstructorInjectedController;
import guru.springframework.didemo.controller.HelloController;
import guru.springframework.didemo.controller.PropertyInjectedController;
import guru.springframework.didemo.controller.SetterInjectedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DiDemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DiDemoApplication.class, args);

        // Every class marked with one of the Spring Annotations that classify that class as a Bean, such as
        // @Controller, @Component, @Repository, are registered in the Spring context with the camelCase (not
        // PascalCase) and then can be fetched by using `getBean`
        HelloController ctrl = (HelloController) ctx.getBean("helloController");

        System.out.println(ctrl.hello());

        // Another way to get our Bean is to pass the class to `getBean`
        System.out.println(ctx.getBean(PropertyInjectedController.class).sayHello());
        System.out.println(ctx.getBean(SetterInjectedController.class).sayHello());
        System.out.println(ctx.getBean(ConstructorInjectedController.class).sayHello());
    }

}

