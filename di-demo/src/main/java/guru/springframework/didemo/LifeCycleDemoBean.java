package guru.springframework.didemo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class LifeCycleDemoBean implements InitializingBean, DisposableBean, BeanNameAware, BeanFactoryAware,
        ApplicationContextAware, CustomBeanProcessor.CustomBean {

    public LifeCycleDemoBean() {
        print("Constructor", "There's nothing that could come before this");
    }

    private void print(String methodName) {
        System.out.println(String.format("## LifeCycleDemoBean.%s", methodName));
    }

    private void print(String methodName, String message) {
        System.out.println(String.format("## LifeCycleDemoBean.%s => %s", methodName, message));
    }

    @Override
    public void setBeanName(String s) {
        print("setBeanName");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        print("setBeanFactory");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        print("setApplicationContext");
    }

    @Override
    public void beforeInit() {
        print("beforeInit", "Called by the bean processor that uses this one");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        print("afterPropertiesSet");
    }

    @PostConstruct
    public void postConstruct() {
        print("postConstruct", "Method annotated with @PostConstruct");
    }

    @Override
    public void afterInit() {
        print("afterInit", "Called by the bean processor that uses this one");
    }

    @PreDestroy
    public void preDestroy() {
        print("preDestroy", "Method annotated with @PreDestroy");
    }

    @Override
    public void destroy() throws Exception {
        print("destroy");
    }

}
