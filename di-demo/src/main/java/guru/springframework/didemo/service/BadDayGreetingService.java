package guru.springframework.didemo.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("bad")
public class BadDayGreetingService implements GreetingService {
    public static final String GREETING = "HELLO WORLD, WHAT A BAD DAY!";

    @Override
    public String greeting() {
        return GREETING;
    }
}
