package guru.springframework.didemo.service;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary // Primary is useful when we want a bean to be considered as default among many, which doesn't overwrites qualified dependencies
@Profile({"en", "default"}) // Profiles are used to specify something like a kind and we say which kind we want through
// spring.profiles.active on our application properties. When we have a profile on all our implementations we have to specify
// one in the properties, otherwise Spring wouldn't know which to use, however, the default profile is the active profile
// when none is specified, thus we can declare that a Bean inhabits more than one profile by passing an array instead of a string
public class GenericGreetingService implements GreetingService {
    @Override
    public String greeting() {
        return "HELLO WORLD!";
    }
}
