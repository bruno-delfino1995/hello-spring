package guru.springframework.didemo.service;

import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService {
    public static final String GREETING = "HELLO WORLD FROM IMPL!";

    @Override
    public String greeting() {
        return GREETING;
    }
}
