package guru.springframework.didemo.service;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("de")
public class GermanGenericGreetingService implements GreetingService {
    @Override
    public String greeting() {
        return "HALLO WELT!";
    }
}
