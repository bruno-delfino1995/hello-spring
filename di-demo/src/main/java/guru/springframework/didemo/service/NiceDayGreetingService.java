package guru.springframework.didemo.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("nice")
public class NiceDayGreetingService implements GreetingService {
    public static final String GREETING = "HELLO WORLD, WHAT A NICE DAY!";

    @Override
    public String greeting() {
        return GREETING;
    }
}
