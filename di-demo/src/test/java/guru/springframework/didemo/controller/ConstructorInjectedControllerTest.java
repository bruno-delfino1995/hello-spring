package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConstructorInjectedControllerTest {
    private ConstructorInjectedController ctrl;

    @Before
    public void setUp() {
        // In means of object creation, this is the most preferred approach because there's no way to create an invalid object,
        // an object that when operated upon would throw and NPE (NullPointerException) because it didn't have the necessary
        // dependencies.
        ctrl = new ConstructorInjectedController(new GreetingServiceImpl());
    }

    @Test
    public void testGreeting() {
        assertEquals(GreetingServiceImpl.GREETING, ctrl.sayHello());
    }
}