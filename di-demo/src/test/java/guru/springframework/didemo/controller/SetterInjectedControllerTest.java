package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingService;
import guru.springframework.didemo.service.GreetingServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SetterInjectedControllerTest {
    private SetterInjectedController ctrl;

    @Before
    public void setUp() {
        ctrl = new SetterInjectedController();
        // The brittleness during object construction in setter based injection isn't removed, however, we add the traceability.
        // But forgetting that, there's quite arguing in the community to choose between this or construction based approach,
        // the main argument is that your injection could fail on construction based approaches if the class isn't available
        // when the dependency is being created, such when you forget to annotate your class.
        ctrl.setService(new GreetingServiceImpl());
    }

    @Test
    public void testGreeting() {
        assertEquals(GreetingServiceImpl.GREETING, ctrl.sayHello());
    }
}