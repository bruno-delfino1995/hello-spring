package guru.springframework.didemo.controller;

import guru.springframework.didemo.service.GreetingServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyInjectedControllerTest {
    private PropertyInjectedController ctrl;

    @Before
    public void setUp() {
        ctrl = new PropertyInjectedController();
        // Either, by property injecting or by injecting through setter makes the object construction a little more
        // brittle because we could break our test just by commenting the below line. However, property based injection
        // is much more brittle because we don't have the traceability given that everything is passed by reference in
        // Java, and that objects are mutable things, so the mutability that is supposed to be enclosed in the class
        // goes to anywhere you can think of.
        ctrl.service = new GreetingServiceImpl();
    }

    @Test
    public void testGreeting() {
        assertEquals(GreetingServiceImpl.GREETING, ctrl.sayHello());
    }

}